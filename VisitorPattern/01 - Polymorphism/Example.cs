﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VisitorPattern.Polymorphism
{
    public class Example
    {
        // Polymorphism
        // single dispatch
        public static void Run()
        {
            IShape shape1 = new Circle { Radius = 10f };
            IShape shape2 = new Rect { Width = 5f, Height = 10f };

            float area1 = shape1.GetArea();
            float area2 = shape2.GetArea();

            // collection
            List<IShape> shapes = new List<IShape>() { shape1, shape2 };
            float area3 = 0f;
            foreach (var shape in shapes)
                area3 += shape.GetArea();

            Console.WriteLine($"area1 == {area1}; area2 == {area2}; area3 == {area3}");
        }
    }
}
