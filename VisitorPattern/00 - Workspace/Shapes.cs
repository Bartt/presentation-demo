﻿using System;

namespace VisitorPattern.Workspace
{
    public interface IShape
    {
    }

    public class Circle : IShape
    {
        public float Radius { get; set; }
    }

    public class Rect : IShape
    {
        public float Width { get; set; }
        public float Height { get; set; }
    }
}
