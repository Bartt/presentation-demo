﻿using System;

namespace VisitorPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Workspace.Example.Run();
            //Polymorphism.Example.Run();
            //PolymorphicSerialization.Example.Run();
            //ExternalSerializer.Example.Run();
            //Visitor.Example.Run();

            Console.WriteLine("Press any key to quit...");
            Console.ReadKey();
        }
    }
}
