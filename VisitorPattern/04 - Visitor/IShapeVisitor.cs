﻿namespace VisitorPattern.Visitor
{
    public interface IShapeVisitor
    {
        void VisitCircle(Circle circle);
        void VisitRect(Rect rect);
    }
}
