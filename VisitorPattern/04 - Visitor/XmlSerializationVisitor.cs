﻿using System.Collections.Generic;
using System.Text;

namespace VisitorPattern.Visitor
{
    public class XmlSerializationVisitor : IShapeSerializer, IShapeVisitor
    {
        private StringBuilder _buffer;

        public string Serialize(IShape shape)
        {
            _buffer = new StringBuilder();
            _buffer.AppendLine("<shapes>");

            shape.Accept(this);

            _buffer.AppendLine("</shapes>");
            return _buffer.ToString();
        }

        public string Serialize(IEnumerable<IShape> shapes)
        {
            _buffer = new StringBuilder();
            _buffer.AppendLine("<shapes>");

            foreach (var shape in shapes)
                shape.Accept(this);

            _buffer.AppendLine("</shapes>");
            return _buffer.ToString();
        }

        public void VisitCircle(Circle circle)
        {
            _buffer.AppendLine($"<circle radius=\"{circle.Radius}\" />");
        }

        public void VisitRect(Rect rect)
        {
            _buffer.AppendLine($"<rect width=\"{rect.Width}\" height=\"{rect.Height}\" />");
        }
    }
}
