﻿using System;
using System.Collections.Generic;

namespace VisitorPattern.Visitor
{
    public class AreaCalculationVisitor : IShapeVisitor
    {
        public float Area { get; set; }

        public float Calculate(IEnumerable<IShape> shapes)
        {
            Area = 0;
            foreach (var shape in shapes)
                shape.Accept(this);

            return Area;
        }

        public void VisitCircle(Circle circle)
        {
            Area += circle.Radius * circle.Radius * MathF.PI;
        }

        public void VisitRect(Rect rect)
        {
            Area += rect.Width * rect.Height;
        }
    }
}
