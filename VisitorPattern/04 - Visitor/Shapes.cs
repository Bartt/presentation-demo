﻿using System;

namespace VisitorPattern.Visitor
{
    public interface IShape
    {
        float GetArea();
        void Accept(IShapeVisitor visitor);
    }

    public class Circle : IShape
    {
        public float Radius { get; set; }

        public float GetArea() => Radius * Radius * MathF.PI;
        public void Accept(IShapeVisitor visitor) => visitor.VisitCircle(this);
    }

    public class Rect : IShape
    {
        public float Width { get; set; }
        public float Height { get; set; }

        public float GetArea() => Width * Height;
        public void Accept(IShapeVisitor visitor) => visitor.VisitRect(this);
    }
}
