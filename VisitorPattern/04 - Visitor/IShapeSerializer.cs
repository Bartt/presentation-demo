﻿using System.Collections.Generic;

namespace VisitorPattern.Visitor
{
    public interface IShapeSerializer
    {
        string Serialize(IShape shape);
        string Serialize(IEnumerable<IShape> shapes);
    }
}
