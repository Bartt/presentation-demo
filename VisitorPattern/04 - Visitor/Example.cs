﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VisitorPattern.Visitor
{
    public class Example
    {
        // Visitor pattern
        // double dispatch
        public static void Run()
        {
            IShape shape1 = new Circle { Radius = 10f };
            IShape shape2 = new Rect { Width = 5f, Height = 10f };
            IShapeSerializer serializer = new XmlSerializationVisitor();

            string xml1 = serializer.Serialize(shape1);
            string xml2 = serializer.Serialize(shape2);

            // collection
            List<IShape> shapes = new List<IShape>() { shape1, shape2 };
            string xml3 = serializer.Serialize(shapes);

            Console.WriteLine($"xml1:\n{xml1}");
            Console.WriteLine($"xml2:\n{xml2}");
            Console.WriteLine($"xml3:\n{xml3}");

            // Area calculation with AreaCalculationVisitor
            AreaCalculationVisitor visitor = new AreaCalculationVisitor();
            shape1.Accept(visitor);
            shape2.Accept(visitor);
            float twoConcreteShapesArea = visitor.Area;

            visitor.Area = 0;
            foreach (IShape shape in shapes)
                shape.Accept(visitor);
            float shapesAreaOnList = visitor.Area;

            Console.WriteLine($"Area of two concrete shapes == {twoConcreteShapesArea};\nArea of all shapes on a list == {shapesAreaOnList}");
        }
    }
}
