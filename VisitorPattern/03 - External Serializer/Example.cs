﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VisitorPattern.ExternalSerializer
{
    public class Example
    {
        // Polymorphism
        // single dispatch
        public static void Run()
        {
            IShape shape1 = new Circle { Radius = 10f };
            IShape shape2 = new Rect { Width = 5f, Height = 10f };
            IShapeSerializer serializer = new XmlSerializer();

            string xml1 = serializer.Serialize(shape1);
            string xml2 = serializer.Serialize(shape2);

            // collection
            List<IShape> shapes = new List<IShape>() { shape1, shape2 };
            string xml3 = serializer.Serialize(shapes);

            Console.WriteLine($"xml1:\n{xml1}");
            Console.WriteLine($"xml2:\n{xml2}");
            Console.WriteLine($"xml3:\n{xml3}");
        }
    }
}
