﻿using System;

namespace VisitorPattern.ExternalSerializer
{
    public interface IShape
    {
        float GetArea();
    }

    public class Circle : IShape
    {
        public float Radius { get; set; }

        public float GetArea() => Radius * Radius * MathF.PI;
    }

    public class Rect : IShape
    {
        public float Width { get; set; }
        public float Height { get; set; }

        public float GetArea() => Width * Height;
    }
}
