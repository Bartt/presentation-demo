﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VisitorPattern.ExternalSerializer
{
    public class XmlSerializer : IShapeSerializer
    {
        public string Serialize(IShape shape)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<shapes>");

            sb.AppendLine(SerializeShape(shape));

            sb.AppendLine("</shapes>");
            return sb.ToString();
        }

        public string Serialize(IEnumerable<IShape> shapes)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<shapes>");

            foreach (var shape in shapes)
                sb.AppendLine(SerializeShape(shape));

            sb.AppendLine("</shapes>");
            return sb.ToString();
        }

        private string SerializeShape(IShape shape)
        {
            // Downcasting. So, that is a problem!
            // LSP violation
            switch (shape)
            {
                case Circle circle:
                    return $"<circle radius=\"{circle.Radius}\" />";

                case Rect rect:
                    return $"<rect width=\"{rect.Width}\" height=\"{rect.Height}\" />";

                default:
                    throw new NotImplementedException($"XmlSerializer doesn't have implementation to serialize shape of type '{shape.GetType()}'");
            }

            //return string.Empty;
        }
    }
}
