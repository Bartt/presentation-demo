﻿using System.Collections.Generic;

namespace VisitorPattern.ExternalSerializer
{
    public interface IShapeSerializer
    {
        string Serialize(IShape shape);
        string Serialize(IEnumerable<IShape> shapes);
    }
}
