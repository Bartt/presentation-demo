﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VisitorPattern.PolymorphicSerialization
{
    public class Example
    {
        // Polymorphism (SRP violation)
        // single dispatch
        public static void Run()
        {
            IShape shape1 = new Circle { Radius = 10f };
            IShape shape2 = new Rect { Width = 5f, Height = 10f };

            string xml1 = SerializeToXml(shape1);
            string xml2 = SerializeToXml(shape2);

            // collection
            List<IShape> shapes = new List<IShape>() { shape1, shape2 };
            string xml3 = SerializeToXml(shapes);

            Console.WriteLine($"xml1:\n{xml1}");
            Console.WriteLine($"xml2:\n{xml2}");
            Console.WriteLine($"xml3:\n{xml3}");
        }

        public static string SerializeToXml(IShape shape)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<shapes>");

            sb.AppendLine(shape.ToXml());

            sb.AppendLine("</shapes>");
            return sb.ToString();
        }

        public static string SerializeToXml(IEnumerable<IShape> shapes)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<shapes>");

            foreach (var shape in shapes)
                sb.AppendLine(shape.ToXml());

            sb.AppendLine("</shapes>");
            return sb.ToString();
        }
    }
}
