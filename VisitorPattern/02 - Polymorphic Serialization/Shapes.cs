﻿using System;

namespace VisitorPattern.PolymorphicSerialization
{
    public interface IShape
    {
        float GetArea();
        string ToXml();
    }

    public class Circle : IShape
    {
        public float Radius { get; set; }

        public float GetArea() => Radius * Radius * MathF.PI;
        public string ToXml() => $"<circle radius=\"{Radius}\" />";
    }

    public class Rect : IShape
    {
        public float Width { get; set; }
        public float Height { get; set; }

        public float GetArea() => Width * Height;
        public string ToXml() => $"<rect width=\"{Width}\" height=\"{Height}\" />";
    }
}
