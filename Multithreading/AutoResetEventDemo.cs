﻿using System;
using System.Threading;

namespace Multithreading
{
    public class AutoResetEventDemo
    {
        private static AutoResetEvent _event;

        public void Run()
        {
            _event = new AutoResetEvent(false);

            Thread[] threads = MultithreadingUtils.StartThreads(3, ThreadProc);

            Console.WriteLine("Wait 3 seconds...");
            Thread.Sleep(3000);
            Console.WriteLine("Go!");
            _event.Set();

            MultithreadingUtils.WaitForAll(threads);
            _event.Dispose();
        }

        private void ThreadProc()
        {
            _event.WaitOne();
            Console.WriteLine($"Started thread #{Thread.CurrentThread.ManagedThreadId}");
            Thread.Sleep(2000);
            Console.WriteLine($"Completed thread #{Thread.CurrentThread.ManagedThreadId}");
            _event.Set();
        }
    }

    /* Output:
        Wait 3 seconds...
        Go!
        Started thread #3
        Completed thread #3
        Started thread #4
        Completed thread #4
        Started thread #5
        Completed thread #5
     * */
}
