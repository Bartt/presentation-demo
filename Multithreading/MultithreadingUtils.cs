﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Multithreading
{
    public static class MultithreadingUtils
    {
        public static bool WaitForAll(Thread[] threads, int ms = -1)
        {
            if (ms < 0)
            {
                foreach (Thread t in threads)
                    t.Join();

                return true;
            }

            foreach (Thread t in threads)
            {
                if (!t.IsAlive)
                    continue;

                if (!t.Join(ms))
                    return false;
            }

            return true;
        }

        public static Thread[] StartThreads(int amount, ThreadStart start, Action<Thread> configure = null)
        {
            Thread[] threads = new Thread[amount];
            for (int i = 0; i < amount; i++)
            {
                Thread t = new Thread(start);
                if (configure != null)
                    configure(t);
                threads[i] = t;
                t.Start();
            }

            return threads;
        }

        public static Thread[] RunParallel(params ThreadStart[] proc)
        {
            Thread[] threads = new Thread[proc.Length];
            for (int i = 0; i < proc.Length; i++)
            {
                Thread t = new Thread(proc[i]);
                threads[i] = t;
                t.Start();
            }

            return threads;
        }
    }
}
