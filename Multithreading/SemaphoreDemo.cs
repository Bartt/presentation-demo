﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Multithreading
{
    public class SemaphoreDemo
    {
        private static Semaphore _semaphore;

        public void Run()
        {
            // Unlike lock (Monitor) and Mutex, Semaphore has no “owner” — it’s thread-agnostic. 
            // Any thread can call Release on a Semaphore, whereas with Mutex and lock, only the thread that obtained the lock can release it.
            // 
            // Initial value can be used to initiate number of requests for the semaphore that can be granted concurrently. it sets your currently available concurrency level for related sempahore.
            // While maximum count sets maximum number of requests for the semaphore that can be granted concurrently. it sets your maximum potential concurrency for related semaphore.
            _semaphore = new Semaphore(3, 5);
            //_semaphore.Release();
            //_semaphore.Release();

            Thread[] threads = MultithreadingUtils.StartThreads(10, ThreadProc);

            MultithreadingUtils.WaitForAll(threads);
            _semaphore.Dispose();
        }

        private void ThreadProc()
        {
            _semaphore.WaitOne();
            Console.WriteLine($"Started thread #{Thread.CurrentThread.ManagedThreadId}");
            Thread.Sleep(3000);
            Console.WriteLine($"Completed thread #{Thread.CurrentThread.ManagedThreadId}");
            _semaphore.Release();
        }
    }

    /*
        Started thread #4
        Started thread #3
        Started thread #5
        -- lag --
        Completed thread #4
        Started thread #6
        Completed thread #3
        Completed thread #5
        Started thread #7
        Started thread #8
        -- lag --
        Completed thread #6
        Started thread #9
        Completed thread #7
        Started thread #10
        Completed thread #8
        Started thread #11
        -- lag --
        Completed thread #9
        Started thread #12
        Completed thread #10
        Completed thread #11
        -- lag --
        Completed thread #12
     * */
}
