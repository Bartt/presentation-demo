﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Multithreading
{
    public class MonitorDemo
    {
        private static object _syncRoot = new object();
        private static int _counter = 0;

        public void Run()
        {
            Thread[] threads = MultithreadingUtils.StartThreads(3, ThreadProc1);

            MultithreadingUtils.WaitForAll(threads);
        }

        // Data race
        private void ThreadProc0()
        {
            for (int i = 0; i < 10; i++)
            {
                _counter++;
                Console.WriteLine($"Thread #{Thread.CurrentThread.ManagedThreadId}; Counter = {_counter}");

                Thread.Sleep(10);
            }
        }

        private void ThreadProc1()
        {
            for (int i = 0; i < 10; i++)
            {
                try
                {
                    Monitor.Enter(_syncRoot);
                    _counter++;
                    Console.WriteLine($"Thread #{Thread.CurrentThread.ManagedThreadId}; Counter = {_counter}");
                }
                finally
                {
                    Monitor.Exit(_syncRoot);
                }

                Thread.Sleep(10);
            }
        }

        private void ThreadProc2()
        {
            for (int i = 0; i < 10; i++)
            {
                lock (_syncRoot)
                {
                    _counter++;
                    Console.WriteLine($"Thread #{Thread.CurrentThread.ManagedThreadId}; Counter = {_counter}");
                }

                Thread.Sleep(10);
            }
        }

    }
}

/* ThreadProc0 Output:
    Thread #3; Counter = 1
    Thread #5; Counter = 3
    Thread #4; Counter = 2
    Thread #3; Counter = 5
    Thread #4; Counter = 5
    Thread #5; Counter = 6
    Thread #3; Counter = 8
    Thread #4; Counter = 8
    Thread #5; Counter = 9
    Thread #5; Counter = 11
    Thread #3; Counter = 11
    Thread #4; Counter = 11
    Thread #5; Counter = 13
    Thread #4; Counter = 13
    Thread #3; Counter = 13
    Thread #5; Counter = 14
    Thread #4; Counter = 16
    Thread #3; Counter = 16
    Thread #5; Counter = 17
    Thread #3; Counter = 18
    Thread #4; Counter = 19
    Thread #3; Counter = 22
    Thread #5; Counter = 21
    Thread #4; Counter = 21
    Thread #4; Counter = 24
    Thread #3; Counter = 24
    Thread #5; Counter = 25
    Thread #5; Counter = 27
    Thread #3; Counter = 27
    Thread #4; Counter = 28

 * ThreadProc1 and ThreadProc2 Output:
    Thread #3; Counter = 1
    Thread #4; Counter = 2
    Thread #5; Counter = 3
    Thread #3; Counter = 4
    Thread #4; Counter = 5
    Thread #5; Counter = 6
    Thread #3; Counter = 7
    Thread #5; Counter = 8
    Thread #4; Counter = 9
    Thread #3; Counter = 10
    Thread #5; Counter = 11
    Thread #4; Counter = 12
    Thread #3; Counter = 13
    Thread #4; Counter = 14
    Thread #5; Counter = 15
    Thread #3; Counter = 16
    Thread #5; Counter = 17
    Thread #4; Counter = 18
    Thread #3; Counter = 19
    Thread #5; Counter = 20
    Thread #4; Counter = 21
    Thread #3; Counter = 22
    Thread #5; Counter = 23
    Thread #4; Counter = 24
    Thread #3; Counter = 25
    Thread #4; Counter = 26
    Thread #5; Counter = 27
    Thread #3; Counter = 28
    Thread #5; Counter = 29
    Thread #4; Counter = 30
 * */
