﻿using System;

namespace Multithreading
{
    class Program
    {
        static void Main(string[] args)
        {
            //new ConcurencyAtGlance().Run();
            //new RaceCondition().Run();
            //new MonitorDemo().Run();
            //new SemaphoreDemo().Run();
            //new SemaphoreDeadlockDemo().Run();
            //new ManualResetEventDemo().Run();
            //new AutoResetEventDemo().Run();
            //new MutexDemo().Run();
            //new BarrierDemo().Run();
            //new InterlockedDemo().Run();

            //new Racing().Run();

            Console.WriteLine("Press any key to quit...");
            Console.ReadKey();
        }
    }
}
