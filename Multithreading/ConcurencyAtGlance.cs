﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Multithreading
{
    public class ConcurencyAtGlance
    {
        private Thread _thread1;
        private Thread _thread2;
        private volatile int _counter = 0;

        public void Run()
        {
            _thread1 = new Thread(ThreadProc);
            _thread2 = new Thread(ThreadProc);

            _thread1.Start();
            _thread2.Start();

            _thread1.Join();
            _thread2.Join();
        }


        private void ThreadProc()
        {
            int tid = Thread.CurrentThread.ManagedThreadId;

            for (int i = 0; i < 10; i++)
            {
                _counter = _counter + 1;
                Thread.Sleep(10);
                Console.WriteLine($"Thread: {tid}; Counter = {_counter}");
            }
        }
    }

    /* Output:
        Thread: 3; Counter = 2
        Thread: 4; Counter = 2
        Thread: 3; Counter = 4
        Thread: 4; Counter = 5
        Thread: 3; Counter = 6
        Thread: 4; Counter = 7
        Thread: 3; Counter = 8
        Thread: 4; Counter = 9
        Thread: 3; Counter = 10
        Thread: 4; Counter = 11
        Thread: 3; Counter = 12
        Thread: 4; Counter = 13
        Thread: 3; Counter = 14
        Thread: 4; Counter = 15
        Thread: 3; Counter = 16
        Thread: 4; Counter = 17
        Thread: 3; Counter = 18
        Thread: 4; Counter = 19
        Thread: 3; Counter = 20
        Thread: 4; Counter = 20
        press any key to quit...
     */
}



/*
    Foreground threads have the ability to prevent the current application from terminating.
    The CLR will not shut down an application (which is to say, unload the hosting AppDomain) until all foreground threads have ended.

    Background threads (sometimes called daemon threads) are viewed by the CLR as expendable paths of execution that can be ignored at any point in time (even if they are currently laboring over some unit of work).
    Thus, if all foreground threads have terminated, any and all background threads are automatically killed when the application domain unloads.
    
    It is important to note that foreground and background threads are not synonymous with primary and worker threads.
    By default, every thread you create via the Thread.Start() method is automatically a foreground thread. Again, this means that the AppDomain will not unload until all threads of execution have completed their units of work.
    In most cases, this is exactly the behavior you require.
*/
