﻿using System;
using System.Threading;

namespace Multithreading
{
    public class MutexDemo
    {
        private static Mutex _mutex1;
        private static Mutex _mutex2;

        public void Run()
        {
            _mutex1 = new Mutex(false);
            _mutex2 = new Mutex(false);

            //Thread[] threads = MultithreadingUtils.StartThreads(3, ThreadProc1);
            Thread[] threads = MultithreadingUtils.RunParallel(ThreadProc2, ThreadProc3); // Deadlock


            MultithreadingUtils.WaitForAll(threads);
            _mutex1.Dispose();
            _mutex2.Dispose();
        }

        private void ThreadProc1()
        {
            _mutex1.WaitOne();
            Console.WriteLine($"Started thread #{Thread.CurrentThread.ManagedThreadId}");
            Thread.Sleep(2000);
            Console.WriteLine($"Completed thread #{Thread.CurrentThread.ManagedThreadId}");
            _mutex1.ReleaseMutex();
        }

        private void ThreadProc2()
        {
            _mutex1.WaitOne();
            Console.WriteLine($"Started thread #{Thread.CurrentThread.ManagedThreadId}");
            Thread.Sleep(2000);

            _mutex2.WaitOne();
            Console.WriteLine($"Completed thread #{Thread.CurrentThread.ManagedThreadId}");
            _mutex2.ReleaseMutex();

            _mutex1.ReleaseMutex();
        }

        private void ThreadProc3()
        {
            _mutex2.WaitOne();
            Console.WriteLine($"Started another job. thread #{Thread.CurrentThread.ManagedThreadId}");
            Thread.Sleep(2000);

            _mutex1.WaitOne();
            Console.WriteLine($"Completed another job. thread #{Thread.CurrentThread.ManagedThreadId}");
            _mutex1.ReleaseMutex();

            _mutex2.ReleaseMutex();

        }
    }

    /* Output:
        Started thread #3
        Completed thread #3
        Started thread #4
        Completed thread #4
        Started thread #5
        Completed thread #5
     * */
}
