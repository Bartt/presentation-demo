﻿using System;
using System.Threading;

namespace Multithreading
{
    public class BarrierDemo
    {
        private static Barrier _barrier;

        public void Run()
        {
            _barrier = new Barrier(3);

            Thread[] threads = MultithreadingUtils.StartThreads(3, ThreadProc);

            MultithreadingUtils.WaitForAll(threads);
            _barrier.Dispose();
        }

        private void ThreadProc()
        {
            Console.WriteLine($"Started thread #{Thread.CurrentThread.ManagedThreadId}");
            _barrier.SignalAndWait();

            Console.WriteLine($"Thread #{Thread.CurrentThread.ManagedThreadId} on stage 1");
            Thread.Sleep(1000);
            _barrier.SignalAndWait();

            Console.WriteLine($"Thread #{Thread.CurrentThread.ManagedThreadId} on stage 2");
            Thread.Sleep(1000);
            _barrier.SignalAndWait();

            Console.WriteLine($"Completed thread #{Thread.CurrentThread.ManagedThreadId}");
        }
    }

    /* Output:
        Started thread #3
        Started thread #4
        Started thread #5
        Thread #3 on stage 1
        Thread #4 on stage 1
        Thread #5 on stage 1
        Thread #5 on stage 2
        Thread #3 on stage 2
        Thread #4 on stage 2
        Completed thread #4
        Completed thread #5
        Completed thread #3
     * */
}
