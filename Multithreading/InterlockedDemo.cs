﻿using System;
using System.Threading;

namespace Multithreading
{
    public class InterlockedDemo
    {
        private static int _counter = 0;

        public void Run()
        {
            Thread[] threads = MultithreadingUtils.StartThreads(3, ThreadProc0);

            MultithreadingUtils.WaitForAll(threads);
        }

        // Race condition
        private void ThreadProc0()
        {
            for (int i = 0; i < 10; i++)
            {
                int val = Interlocked.Increment(ref _counter);
                Console.WriteLine($"Thread #{Thread.CurrentThread.ManagedThreadId}; Counter = {val}");

                Thread.Sleep(10);
            }
        }
    }
}

/* ThreadProc0 Output:
    Thread #3; Counter = 1
    Thread #5; Counter = 2
    Thread #4; Counter = 3
    Thread #3; Counter = 6
    Thread #5; Counter = 5
    Thread #4; Counter = 4
    Thread #4; Counter = 8
    Thread #5; Counter = 9
    Thread #3; Counter = 7
    Thread #3; Counter = 10
    Thread #5; Counter = 11
    Thread #4; Counter = 12
    Thread #5; Counter = 14
    Thread #3; Counter = 13
    Thread #4; Counter = 15
    Thread #5; Counter = 16
    Thread #4; Counter = 18
    Thread #3; Counter = 17
    Thread #5; Counter = 19
    Thread #3; Counter = 20
    Thread #4; Counter = 21
    Thread #5; Counter = 22
    Thread #4; Counter = 23
    Thread #3; Counter = 24
    Thread #5; Counter = 25
    Thread #3; Counter = 27
    Thread #4; Counter = 26
    Thread #5; Counter = 28
    Thread #4; Counter = 29
    Thread #3; Counter = 30
 * */
