﻿using System;
using System.Threading;

namespace Multithreading
{
    public class SemaphoreDeadlockDemo
    {
        private static Semaphore _semaphore;

        public void Run()
        {
            _semaphore = new Semaphore(1, 1);

            Thread[] threads = MultithreadingUtils.StartThreads(1, ThreadProc);

            MultithreadingUtils.WaitForAll(threads);
            _semaphore.Dispose();
        }

        private void ThreadProc()
        {
            _semaphore.WaitOne();
            Console.WriteLine($"Started thread #{Thread.CurrentThread.ManagedThreadId}");
            Thread.Sleep(1000);

            Console.WriteLine($"Trying to handle semaphore another time");
            _semaphore.WaitOne();
            Console.WriteLine($"It works!!!");
            _semaphore.Release();

            Console.WriteLine($"Completed thread #{Thread.CurrentThread.ManagedThreadId}");
            _semaphore.Release();
        }
    }

    /* Output:
        Started thread #3
        Trying to handle semaphore another time
     * */
}
