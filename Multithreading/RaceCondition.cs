﻿using System;
using System.Threading;

namespace Multithreading
{
    public class RaceCondition
    {
        private Thread _thread1;
        private Thread _thread2;
        private static volatile int _counter = 0;
        private static volatile bool _stop = false;

        public void Run()
        {
            _thread1 = new Thread(ThreadProc1);
            _thread2 = new Thread(ThreadProc2);
            _thread1.IsBackground = true;
            _thread2.IsBackground = true;

            _thread1.Start();
            _thread2.Start();

            Console.ReadKey();
            _stop = true;
            _thread1.Join();
            _thread2.Join();
        }


        private static void ThreadProc1()
        {
            while (!_stop)
                _counter++;
        }

        private static void ThreadProc2()
        {
            while (!_stop)
                if (_counter % 2 == 0)
                    Console.WriteLine($"Counter = {_counter}");
        }
    }

    /* Output:
     *  Counter = 103535324
        Counter = 103702638
        Counter = 103831126
        Counter = 103954875
        Counter = 103993766
        Counter = 105106909
        Counter = 105158435
        Counter = 105518482
        Counter = 105527071
        Counter = 105867554
        Counter = 105882036
        Counter = 106076718
        Counter = 106095453
        Counter = 106376522
        Counter = 106379600
     * */
}
