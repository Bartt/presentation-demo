﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Multithreading
{
    public class Racing
    {
        private static ManualResetEvent _event;

        private class Account
        {
            public int Balance { get; set; }
        }

        private enum TransferResult
        {
            Failed,
            Succeeded
        }

        private delegate TransferResult TransferDelegate(int amount, Account accountFrom, Account accountTo);

        private Account AccountA { get; set; } = new Account();
        private Account AccountB { get; set; } = new Account();
        private Account AccountC { get; set; } = new Account();
        private TransferDelegate TransferImplementation;

        public void Run()
        {
            // initialize
            AccountA.Balance = 10;
            TransferImplementation = Transfer1;

            // prepare MT, sync< run and wait result
            _event = new ManualResetEvent(false);

            Thread[] threads = MultithreadingUtils.RunParallel(ThreadProc1, ThreadProc2);

            Console.WriteLine("Wait a seconds...");
            Thread.Sleep(1000);
            Console.WriteLine("Go!");
            _event.Set();
            MultithreadingUtils.WaitForAll(threads);
            _event.Dispose();

            // log result
            Console.WriteLine($"A.balance = {AccountA.Balance}");
        }

        private void ThreadProc1()
        {
            _event.WaitOne();

            var rs = TransferImplementation(10, AccountA, AccountB);
            Console.WriteLine($"A -> B: {rs}");
        }

        private void ThreadProc2()
        {
            _event.WaitOne();

            var rs = TransferImplementation(10, AccountA, AccountC);
            Console.WriteLine($"A -> C: {rs}");
        }

        // Data Race + Race Condition
        private TransferResult Transfer1(int amount, Account accountFrom, Account accountTo)
        {
            if (accountFrom.Balance < amount)
                return TransferResult.Failed;

            Thread.Sleep(1); // simulate context switching
            accountFrom.Balance -= amount;
            accountTo.Balance += amount;
            return TransferResult.Succeeded;
        }

        // Race Condition
        private TransferResult Transfer2(int amount, Account accountFrom, Account accountTo)
        {
            lock (AccountA)
            {
                if (accountFrom.Balance < amount)
                    return TransferResult.Failed;
            }

            Thread.Sleep(1); // simulate context switching

            lock (AccountA)
            {
                accountFrom.Balance -= amount;
            }

            lock (AccountA)
            {
                accountTo.Balance += amount;
            }

            return TransferResult.Succeeded;
        }

        // Synchronized
        private TransferResult Transfer3(int amount, Account accountFrom, Account accountTo)
        {
            lock (AccountA)
            {
                if (accountFrom.Balance < amount)
                    return TransferResult.Failed;

                Thread.Sleep(1); // simulate context switching

                accountFrom.Balance -= amount;
                accountTo.Balance += amount;
                return TransferResult.Succeeded;
            }
        }
    }
}

/*
 * Transfer1:
    A -> C: Succeeded
    A -> B: Succeeded
    A.balance = 0

 * Transfer2:
    A -> C: Succeeded
    A -> B: Succeeded
    A.balance = -10

 * Transfer3:
    A -> C: Succeeded
    A -> B: Succeeded
    A.balance = -10

 */
